package fr.afpa.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.MeteoriteBase;
import fr.afpa.ihms.FenetrePrincipale;
import fr.afpa.services.TirService;

public class TirServiceTest {

	@Test
	void testCollisionMeteorite() {
		FenetrePrincipale fenetre = new FenetrePrincipale("test");
		TirService ts = new TirService(fenetre);
		MeteoriteBase meteorite = new MeteoriteBase("", "ressources/base.jpg", 0, 0, 0, 20, 0, 0);
		meteorite.setLocation(50, 50);
		fenetre.getListeMeteorites().add(meteorite);
		assertTrue(ts.collisionMeteorite(new Rectangle(55, 55, 20, 20)));
		assertFalse(ts.collisionMeteorite(new Rectangle(20, 20, 20, 20)));
	}
}
