package fr.afpa.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.ihms.FenetrePrincipale;
import fr.afpa.services.CreationMeteoriteService;

public class CreationMeteoriteServiceTest {
	@Test
	void testCretionMeteoriteService() {
		CreationMeteoriteService cms = new CreationMeteoriteService(new FenetrePrincipale("test"));
		assertNotNull(cms.creationMeteorite(0));
		assertNotNull(cms.creationMeteorite(1));
		assertNotNull(cms.creationMeteorite(2));
		assertNotNull(cms.creationMeteorite(3));
		assertNotNull(cms.creationMeteorite(4));
		assertNull(cms.creationMeteorite(5));
	}

	@Test
	void testCretionNbMeteorite() {
		CreationMeteoriteService cms = new CreationMeteoriteService(new FenetrePrincipale("test"));
		assertTrue(cms.creationNbMeteorite(1));
		assertFalse(cms.creationNbMeteorite(1));
}
}
