package fr.afpa.services;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entites.Meteorite;
import fr.afpa.entites.MeteoriteBase;
import fr.afpa.entites.Score;
import fr.afpa.ihms.FenetrePrincipale;

public class DeplacementMeteoriteTest {

	private FenetrePrincipale fenetre;
	
	@BeforeEach
	public void setUp() throws Exception {
		fenetre = new FenetrePrincipale("Toto");
		fenetre.getListeMeteorites().add(new MeteoriteBase("meteoriteBase", "ressources/base.jpg", 2, 1, 2, 20, 0, 1));
	}


	@Test
	public void testDeplacementMeteorite() {
		fenetre = new FenetrePrincipale("Toto");
		fenetre.getListeMeteorites().add(new MeteoriteBase("meteoriteBase", "ressources/base.jpg", 2, 1, 2, 20, 0, 1));
		assertTrue(!fenetre.getListeMeteorites().isEmpty());
		assertTrue(fenetre.getScore().getScorePoints() == 0);
		fenetre.getListeMeteorites().get(0).setLocation(fenetre.getListeMeteorites().get(0).getX(), fenetre.getHeight() +10);
		new DeplacementMeteorite(fenetre, fenetre.getListeMeteorites(), fenetre.getScore()).gestionDeplacementMeteorite();
		assertTrue(fenetre.getListeMeteorites().isEmpty());
		assertTrue(fenetre.getScore().getScorePoints() == 2);
	}
	

	@AfterEach
	public void tearDown() throws Exception {
		fenetre = null;
	}
}
