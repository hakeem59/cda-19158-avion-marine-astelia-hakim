package fr.afpa.services;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.JPanel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entites.MeteoriteZigZag;
import fr.afpa.ihms.FenetrePrincipale;

public class DeplacementServiceTest {

	private FenetrePrincipale fenetre;
	private JPanel panel;
	private MeteoriteZigZag meteoriteZigZag;
	
	@BeforeEach
	public void setUp() throws Exception {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		meteoriteZigZag = new MeteoriteZigZag("meteoriteZZ", "ressources/zigzag.jpg", 1, 2, 5, 30, 1, 2);
		fenetre.add(panel);
		fenetre.add(meteoriteZigZag);
	}

	
	@Test
	public void testDeplacerMeteoriteZigZag() {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		meteoriteZigZag = new MeteoriteZigZag("meteoriteZZ", "ressources/zigzag.jpg", 1, 2, 5, 30, 1, 2);
		fenetre.add(panel);
		fenetre.add(meteoriteZigZag);
		int nb = 10;
		int x = meteoriteZigZag.getX();
		int y = meteoriteZigZag.getY();
		assertTrue(meteoriteZigZag.getX() == x);
		assertTrue(meteoriteZigZag.getY() == y);
		meteoriteZigZag.setCompteur(25);
		meteoriteZigZag.setDeplacement(false);
		
		System.out.println("Meteorite : "+meteoriteZigZag.getX()+"   "+meteoriteZigZag.getY());
		DeplacementService.deplacer(fenetre, meteoriteZigZag, nb, nb);
		System.out.println("Meteorite : "+meteoriteZigZag.getX()+"   "+meteoriteZigZag.getY());
		assertTrue(meteoriteZigZag.getX() == x || meteoriteZigZag.getX() == x-nb || meteoriteZigZag.getX() == x+nb);
		meteoriteZigZag.setDeplacement(true);
		
		x = meteoriteZigZag.getX();
		y = meteoriteZigZag.getY();
		DeplacementService.deplacer(fenetre, meteoriteZigZag, nb, nb);
		System.out.println("Meteorite : "+meteoriteZigZag.getX()+"   "+meteoriteZigZag.getY());
		assertTrue(meteoriteZigZag.getX() == x || meteoriteZigZag.getX() == x-nb || meteoriteZigZag.getX() == x+nb);
	}
	
	@Test
	public void testDeplacerPanel() {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		meteoriteZigZag = new MeteoriteZigZag("meteoriteZZ", "ressources/zigzag.jpg", 1, 2, 5, 30, 1, 2);
		fenetre.add(panel);
		fenetre.add(meteoriteZigZag);
		int x = panel.getX();
		int y = panel.getY();
		assertTrue(panel.getX() == x);
		assertTrue(panel.getY() == y);
		DeplacementService.deplacer(fenetre, panel, 25, 25);
		assertTrue(panel.getX() == x+25);
		assertTrue(panel.getY() == y+25);
	}

	
	@AfterEach
	public void tearDown() throws Exception {
		fenetre = null;
		panel = null;
		meteoriteZigZag = null;
	}

}
