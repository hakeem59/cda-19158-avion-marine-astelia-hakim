package fr.afpa.controle;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.JPanel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entites.Avion;
import fr.afpa.ihms.FenetrePrincipale;

public class ControleBorduresTest {

	private FenetrePrincipale fenetre;
	private JPanel panel;
	
	@BeforeEach
	public void setUp() throws Exception {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		fenetre.add(panel);
	}

	
	@Test
	public void testDansLeCadreAvion() {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		fenetre.add(panel);
		// Panel dans le cadre
		assertTrue(ControleBordures.dansLeCadre(fenetre, fenetre.getAvion(), fenetre.getAvion().getX(), fenetre.getAvion().getY()));
		// Panel hors du cadre verticalement
		fenetre.getAvion().setLocation(fenetre.getAvion().getX(), fenetre.getHeight()+1);
		assertFalse(ControleBordures.dansLeCadre(fenetre, fenetre.getAvion(), fenetre.getAvion().getX(), fenetre.getAvion().getY()));
		// Panel dans le cadre
		fenetre.getAvion().setLocation(fenetre.getAvion().getX(), fenetre.getY()+fenetre.getAvion().getHeight());
		assertTrue(ControleBordures.dansLeCadre(fenetre, fenetre.getAvion(), fenetre.getAvion().getX(), fenetre.getAvion().getY()));
		// Panel hors du cadre horizontalement
		fenetre.getAvion().setLocation(fenetre.getWidth()+1, fenetre.getAvion().getY());
		assertFalse(ControleBordures.dansLeCadre(fenetre, fenetre.getAvion(), fenetre.getAvion().getX(), fenetre.getAvion().getY()));
	}
	
	@Test
	public void testDansLeCadreMeteorites() {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		fenetre.add(panel);
		// Panel dans le cadre
		assertTrue(ControleBordures.dansLeCadre(fenetre, panel, panel.getX(), panel.getY()));
		// Panel hors du cadre verticalement
		panel.setLocation(panel.getX(), fenetre.getHeight()+1);
		assertTrue(ControleBordures.dansLeCadre(fenetre, panel, panel.getX(), panel.getY()));
		// Panel dans le cadre
		panel.setLocation(panel.getX(), fenetre.getY()+panel.getHeight());
		assertTrue(ControleBordures.dansLeCadre(fenetre, panel, panel.getX(), panel.getY()));
		// Panel hors du cadre horizontalement
		panel.setLocation(fenetre.getWidth()+1, panel.getY());
		assertFalse(ControleBordures.dansLeCadre(fenetre, panel, panel.getX(), panel.getY()));
	}
	
	@Test
	public void testSousLeBordDuBas() {
		fenetre = new FenetrePrincipale("Bidule");
		panel = new JPanel();
		fenetre.add(panel);
		assertFalse(ControleBordures.sousLeBordDuBas(fenetre, panel));
		panel.setLocation(panel.getX(), fenetre.getHeight()+1);
		assertTrue(ControleBordures.sousLeBordDuBas(fenetre, panel));
	}

	
	@AfterEach
	public void tearDown() throws Exception {
		fenetre = null;
		panel = null;
	}
	
}
