package fr.afpa.controle;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Avion;

public class ControleVieTest {
	
	@Test
	void testEstVivant() {
		Avion avion = new Avion(5);
		assertTrue(ControleVie.estVivant(avion));
		avion.setPv(0);
		assertFalse(ControleVie.estVivant(avion));
		assertFalse(ControleVie.estVivant(null));
		
	}

}
	