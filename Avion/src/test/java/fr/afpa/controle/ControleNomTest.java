package fr.afpa.controle;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ControleNomTest {

	@Test
	public void testNom() {
		assertTrue(ControleNom.nomValide("Toto"));
		assertFalse(ControleNom.nomValide("Toto;"));
		assertFalse(ControleNom.nomValide("trucBidule"));
		assertFalse(ControleNom.nomValide("te"));
		assertFalse(ControleNom.nomValide("@test"));
		assertFalse(ControleNom.nomValide(null));
	}

}
