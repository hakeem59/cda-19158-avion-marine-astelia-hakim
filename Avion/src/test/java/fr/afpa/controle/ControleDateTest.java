package fr.afpa.controle;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.controle.ControleDate;

public class ControleDateTest {
	
	@Test
	void testEstDateValide() {
		assertTrue(ControleDate.estDateValide("2019-12-07T10:55:28.505"));
		assertFalse(ControleDate.estDateValide("2019-112-07T10:55:28.505"));
		
	}

}

