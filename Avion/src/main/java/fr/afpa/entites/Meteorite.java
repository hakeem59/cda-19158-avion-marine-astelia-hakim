package fr.afpa.entites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Meteorite extends JPanel{
	
	protected String nom;
	protected BufferedImage meteorite;
	protected int vitesse;
	protected int degatMeteorite;
	protected int nbrPoint;
	protected int taille;
	protected int dx;//type de deplacement de la meteorite (X)
	protected int dy;//type de deplacement de la meteorite (Y)
	protected boolean meteoritePoint;
	
	
	public Meteorite(String nom, String cheminImage, int vitesse, int degatMeteorite, int nbrPoint, int taille,
			int dx, int dy) {
		super();
		this.nom = nom;
		try {
			this.meteorite = ImageIO.read(new File (cheminImage));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.vitesse = vitesse;
		this.degatMeteorite = degatMeteorite;
		this.nbrPoint = nbrPoint;
		this.taille = taille;
		this.dx = dx;
		this.dy = dy;
		setSize(taille,taille);
		setBackground(Color.BLACK);
		meteoritePoint=true;
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(meteorite.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_AREA_AVERAGING), 0, 0, null);
	}
		
	

}
