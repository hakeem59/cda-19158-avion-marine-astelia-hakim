package fr.afpa.entites;

public class MeteoriteIceberg extends Meteorite {

	public MeteoriteIceberg(String nom, String cheminImage, int vitesse, int degatMeteorite, int nbrPoint, int taille,
			int dx, int dy) {
		super(nom, cheminImage, vitesse, degatMeteorite, nbrPoint, taille, dx, dy);
	}
}