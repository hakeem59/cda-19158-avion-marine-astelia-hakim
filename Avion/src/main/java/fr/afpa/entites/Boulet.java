package fr.afpa.entites;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Boulet extends JPanel{
	
	
	protected BufferedImage imageBoulet;
	
	
	
	public Boulet() {
		super();
		try {
			this.imageBoulet = ImageIO.read(new File ("ressources/explosion.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imageBoulet.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_AREA_AVERAGING), 0, 0, null);
	}

}
