package fr.afpa.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MeteoriteZigZag extends Meteorite {
	
	private boolean deplacement;
	int compteur;

	public MeteoriteZigZag(String nom, String cheminImage, int vitesse, int degatMeteorite, int nbrPoint, int taille,
			int dx, int dy) {
		super(nom, cheminImage, vitesse, degatMeteorite, nbrPoint, taille, dx, dy);
	}
}