package fr.afpa.entites;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Score implements Comparable{

	private String nom;
	private int scorePoints;
	private LocalDateTime date;
	
	public Score(String nom, LocalDateTime date) {
		super();
		this.nom = nom;
		this.scorePoints = 0;
		this.date = date;
	}

	public Score(String nom, int scorePoints, LocalDateTime date) {
		super();
		this.nom = nom;
		this.scorePoints = scorePoints;
		this.date = date;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof Score) {
		return ((Score) o).getScorePoints() - scorePoints;
		}
		return 0;
	}
	
	
	
	
}
