package fr.afpa.entites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


import lombok.Getter;
import lombok.Setter;


	@Getter
	@Setter
public class Avion extends JPanel{
	
	private int pv;
	private BufferedImage avionCentre;
	private BufferedImage avionN;
	private BufferedImage avionE;
	private BufferedImage avionS;
	private BufferedImage avionW;
	private BufferedImage explosion1;
	private BufferedImage explosion2;
	private BufferedImage imageEnCours;
	
	public Avion(int pv)  {
		super();
		this.setBackground(Color.BLACK);
		try {
			 avionCentre= ImageIO.read(new File("ressources/fauconmillenium.png"));
			 avionN = ImageIO.read(new File("ressources/fauconmilleniumN.png"));
			 avionE = ImageIO.read(new File("ressources/fauconmilleniumE.png"));
			 avionS = ImageIO.read(new File("ressources/fauconmilleniumS.png"));
			 avionW = ImageIO.read(new File("ressources/fauconmilleniumW.png"));
			 explosion1 = ImageIO.read(new File("ressources/explosion.png"));
			 explosion2 = ImageIO.read(new File("ressources/explosionCartoon.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.pv=pv;
		imageEnCours=avionCentre;
		
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imageEnCours.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_AREA_AVERAGING), 0, 0, null);
		}
	}


	

	

	
	
	
	


	
	
