package fr.afpa.listeners;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JDialog;

import fr.afpa.controle.ControleBordures;
import fr.afpa.entites.Avion;
import fr.afpa.entites.Boulet;
import fr.afpa.ihms.FenetrePrincipale;
import fr.afpa.services.DeplacementService;

public class MouvementAvionListener implements KeyListener {

	private FenetrePrincipale fenetre;
	private final JDialog pause;

	public MouvementAvionListener(FenetrePrincipale fenetre) {
		this.fenetre = fenetre;
		this.pause = new JDialog(fenetre);
		this.pause.setTitle("Pause");
		this.pause.addKeyListener(this);
		this.pause.addWindowListener(new FermerFenetrePause(fenetre, pause));
		this.pause.setLocation(fenetre.getX() + 125, fenetre.getY() + 275);
		this.pause.pack();
		this.pause.setVisible(false);

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (!fenetre.isPause()) {
			int pas = fenetre.getJPanelFond().getPasIncrementation() * 10;
			int key = e.getKeyCode();

			if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_Q) {
				DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), -pas, 0);
				BufferedImage image = this.fenetre.getAvion().getAvionW();
				this.fenetre.getAvion().setImageEnCours(image);
			}

			if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
				DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), pas, 0);
				BufferedImage image = this.fenetre.getAvion().getAvionE();
				this.fenetre.getAvion().setImageEnCours(image);
			}

			if (key == KeyEvent.VK_UP || key == KeyEvent.VK_Z) {
				DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), 0, -pas);
				BufferedImage image = this.fenetre.getAvion().getAvionN();
				this.fenetre.getAvion().setImageEnCours(image);
			}

			if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
				DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), 0, pas);
				BufferedImage image = this.fenetre.getAvion().getAvionS();
				this.fenetre.getAvion().setImageEnCours(image);
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		synchronized (fenetre) {
			if (e.getKeyCode() == 32 && fenetre.getListeBoulets().size() < 3) {
				Boulet boulet = new Boulet();
				boulet.setSize(10, 10);
				boulet.setLocation(
						fenetre.getAvion().getX() + fenetre.getAvion().getWidth() / 2 - boulet.getWidth() / 2,
						fenetre.getAvion().getY() - boulet.getHeight() / 2);

				boulet.setBackground(Color.BLACK);
				fenetre.getJPanelFond().add(boulet);
				fenetre.getListeBoulets().add(boulet);
			}

			if (e.getKeyCode() == KeyEvent.VK_B) {
				fenetre.setPause(!fenetre.isPause());
				if (fenetre.isPause()) {
					this.pause.setVisible(true);
				} else {
					this.pause.setVisible(false);
				}
			}
		}

		BufferedImage image = this.fenetre.getAvion().getAvionCentre();
		this.fenetre.getAvion().setImageEnCours(image);
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
