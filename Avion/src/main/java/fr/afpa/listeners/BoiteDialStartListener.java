package fr.afpa.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import fr.afpa.ihms.BoiteDialogNom;
import fr.afpa.ihms.FenetreDialDemarrage;
import fr.afpa.ihms.FenetrePrincipale;
import fr.afpa.services.RecuperationTopScoreService;

public class BoiteDialStartListener implements ActionListener {
	private FenetrePrincipale fenetre;
	private FenetreDialDemarrage fdd;
	
	public BoiteDialStartListener(FenetrePrincipale fenetre, FenetreDialDemarrage fdd) {
		super();
		this.fenetre = fenetre;
		this.fdd = fdd;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		BoiteDialogNom jdialogNom = new BoiteDialogNom(fenetre, fdd);
		switch (e.getActionCommand()) {
		case "NEW GAME":
			fdd.setVisible(false);
			jdialogNom.setVisible(true);
			break;

		case "TOP SCORES":
			JOptionPane.showMessageDialog(null, new RecuperationTopScoreService().affichageTop(20), "TOP 20",
					JOptionPane.INFORMATION_MESSAGE);
			break;

		case "QUITTER":
			System.exit(0);
			break;

		default:
			break;
		}

	}
}
