package fr.afpa.listeners;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;

import fr.afpa.ihms.FenetrePrincipale;

public class FermerFenetrePause implements WindowListener {

	private FenetrePrincipale fenetre;
	private JDialog pause;
	
	
	public FermerFenetrePause(FenetrePrincipale fenetre, JDialog pause) {
		this.fenetre = fenetre;
		this.pause = pause;
	}
	
	
	@Override
	public void windowActivated(WindowEvent arg0) {

	}

	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		this.fenetre.setPause(false);
		this.pause.setVisible(false);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
	}

}
