package fr.afpa.listeners;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import fr.afpa.ihms.FenetreDialDemarrage;

public class FermerBoiteNom implements WindowListener {

	private FenetreDialDemarrage fdd;
	
	public FermerBoiteNom(FenetreDialDemarrage fdd) {
		this.fdd = fdd;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		fdd.setVisible(true);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		
	}
	
	

}
