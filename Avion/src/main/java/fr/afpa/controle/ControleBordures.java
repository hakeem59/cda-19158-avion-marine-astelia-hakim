package fr.afpa.controle;

import javax.swing.JPanel;

import fr.afpa.entites.Avion;
import fr.afpa.ihms.FenetrePrincipale;

public class ControleBordures {

	/**
	 * Permet de verifier si le panel ne sortira pas du cadre : si celui-ci est un avion, 
	 * il doit rester dans le cadre du jeu et lorsque c'est une meteorite,
	 * elle ne peut sortir que par les bords du haut et du bas
	 * @param fenetre : la fenetre qui contient le panel
	 * @param panel : le panel dont on souhaite modifier la position
	 * @param dx : l'abcisse a verifier
	 * @param dy : l'ordonnee a verifier
	 * @return true si les coordonnees sont dans le cadre et false sinon
	 */
	public static boolean dansLeCadre(FenetrePrincipale fenetre, JPanel panel, int dx, int dy) {
		int bordureMaxX = fenetre.getWidth() - panel.getWidth();
		int bordureMaxY = fenetre.getHeight() - 40 - panel.getHeight();
		
		if (panel instanceof Avion) {
			return 0<=dx && dx<bordureMaxX && 1<=dy && dy<bordureMaxY;
		}
		else {
			return 0<=dx && dx<bordureMaxX;
		}
		
	}
	
	/**
	 * Permet de verifier si un panel se trouve en dehors du cadre, 
	 * sous le bord du bas
	 * @param fenetre : la fenetre qui contient le panel
	 * @param panel : le panel dont on verifie la position
	 * @return : true si le panel se trouve sous le bord d bas et false sinon
	 */
	public static boolean sousLeBordDuBas(FenetrePrincipale fenetre, JPanel panel) {
		return fenetre.getHeight() < panel.getY();
	}
	
	
}
