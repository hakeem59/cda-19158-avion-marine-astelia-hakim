package fr.afpa.controle;

public class ControleNom {

	/**
	 * Permet de verifie si un nom a une longueur minimale de 3, une longueur
	 * maximale de 6 et n'est constituee que de mots
	 * @param nom : le nom a verifier
	 * @return true si le nom est valide et false sinon
	 */
	public static boolean nomValide(String nom) {
		if (nom != null) {
			return nom.matches("[\\w]{3,6}");
		}
		return false;
	}
	
}
