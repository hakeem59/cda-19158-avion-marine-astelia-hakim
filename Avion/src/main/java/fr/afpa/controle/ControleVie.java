package fr.afpa.controle;

import fr.afpa.entites.Avion;

public class ControleVie {
	
	

	private ControleVie() {
		super();
	}

	/**
	 * Permet de verifier si un avion ea encore des points de vie
	 * @param avion : l'avion dont on veut verifier les points de vie
	 * @return true si l'avion a encore des points de vie et false sinon
	 */
	public static boolean estVivant(Avion avion) {
		if(avion!=null) {
		return avion.getPv()>0;
		}
		return false;
	}
}
