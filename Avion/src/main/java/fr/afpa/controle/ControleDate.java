package fr.afpa.controle;

import java.time.LocalDateTime;

public class ControleDate {

	
	private ControleDate() {
		super();
	}

	/**
	 * Controle si la chaine de caractères est une date valide
	 * @param date : la date à controler
	 * @return true si la chaine de caractères est ube localDateTime, false sinon 
	 */
	public static boolean estDateValide(String date) {
		try {
			LocalDateTime.parse(date);
		}catch (Exception e) {
			return false;
		}
		return true;
	}
}
