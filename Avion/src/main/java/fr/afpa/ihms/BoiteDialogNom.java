package fr.afpa.ihms;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.afpa.listeners.BoutonJouerDialogNomListener;
import fr.afpa.listeners.FermerBoiteNom;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BoiteDialogNom extends JDialog {

	private static final long serialVersionUID = 1L;

	public static final JLabel LABEL_NOM = new JLabel("   Veuillez entrer votre nom (3 a 6 caracteres)   ");
	public static final JButton JOUER = new JButton("Jouer");
	
	private JPanel panel;
	private JLabel messageErreur;
	private JTextField nom;
	private boolean demarrerJeu;
	
	
	public BoiteDialogNom(FenetrePrincipale fenetre, FenetreDialDemarrage fdd) {
		super(fenetre);
		panel = new JPanel(new GridLayout(4, 0));
		
		panel.add(LABEL_NOM);
		
		nom = new JTextField(10);
		panel.add(nom);
		
		JOUER.addActionListener(new BoutonJouerDialogNomListener(fenetre, this, nom));
		panel.add(JOUER);
		
		messageErreur = new JLabel("");
		messageErreur.setSize(30, messageErreur.getHeight());
		panel.add(messageErreur);
		
		this.add(panel);
		this.addWindowListener(new FermerBoiteNom(fdd));
		//this.setLocation(fenetre.getX()+125, fenetre.getY()+275);
		
		this.pack();
		this.setLocationRelativeTo(fenetre);
	}
	
	
	
	
}
