package fr.afpa.ihms;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Avion;
import fr.afpa.entites.Boulet;
import fr.afpa.entites.Meteorite;
import fr.afpa.entites.Score;
import fr.afpa.listeners.MouvementAvionListener;
import fr.afpa.services.CreationMeteoriteService;
import fr.afpa.services.DeplacementMeteorite;
import fr.afpa.services.FichierService;
import fr.afpa.services.TirService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FenetrePrincipale extends JFrame implements Runnable {

	private JPanelImageFond jPanelFond;

	private JPanel jPanelBarreScore;
	private JLabel jLabelNom;
	private JLabel jLabelScore;
	private JLabel jLabelVie;

	private Score score;
	private Avion avion;

	private List<Meteorite> listeMeteorites;
	private List<Boulet> listeBoulets;

	private boolean pause;

	public FenetrePrincipale(String nom) {
		super(nom);

		score = new Score("", LocalDateTime.now());

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(600, 100, 600, 800);
		setResizable(false);

		initialisationComposants();

	}

	/**
	 * Fonction qui lance le thread pour que le jeu ne demarre pas avant que le
	 * joueur ait rentré son nom dans la boite de dialogue
	 */
	public void boucleJeu() {
		Thread jeu = new Thread(this);
		jeu.setPriority(1);
		jeu.start();
	}

	/**
	 * Fonction d'initialisation des composants de la Frame
	 */
	private void initialisationComposants() {

		listeMeteorites = new Vector<>();
		listeBoulets = new Vector<>();

		// Panel du jeu
		jPanelFond = new JPanelImageFond("ressources/fond3.jpg");
		jPanelFond.setFenetre(this);

		// ajout de l'avion au jPanelFond et positionnement avion an bas au milieu
		avion = new Avion(5);
		jPanelFond.add(avion);
		avion.setSize(50, 50);
		avion.setLocation(this.getWidth() / 2 - avion.getWidth(), this.getHeight() - 120);

		getContentPane().add(jPanelFond, BorderLayout.CENTER);

		// Barre du score, nom, vie
		jPanelBarreScore = new JPanel(new GridLayout(1, 3));
		jLabelScore = new JLabel("Score : " + score.getScorePoints());
		jLabelScore.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		jLabelVie = new JLabel("Vie : " + avion.getPv());
		jLabelVie.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));

		jLabelNom = new JLabel("Nom : " + "JOUEUR");
		jLabelNom.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		jPanelBarreScore.add(jLabelScore);
		jPanelBarreScore.add(jLabelNom);
		jPanelBarreScore.add(jLabelVie);

		getContentPane().add(jPanelBarreScore, BorderLayout.NORTH);

		this.addKeyListener(new MouvementAvionListener(this));

	}

	/**
	 * Fonction de démarrage du jeu( lancement des Threads)
	 */
	public void demarrerJeu() {
		jLabelNom.setText("Nom : " + score.getNom());
		avion.setPv(5);
		jLabelVie.setText("Vie : " + avion.getPv());
		avion.setLocation(this.getWidth() / 2 - avion.getWidth(), this.getHeight() - 120);
		score.setScorePoints(0);
		jLabelScore.setText("Score : " + score.getScorePoints());
		score.setDate(LocalDateTime.now());

		// suppression des meteorites de la partie precédente
		for (Meteorite meteorite : listeMeteorites) {
			jPanelFond.remove(meteorite);
		}
		listeMeteorites.clear();

		// suppression des boulets de la partie precédente
		for (Boulet boulet : listeBoulets) {
			jPanelFond.remove(boulet);
		}
		listeBoulets.clear();

		// Thread controlant le défilement du fond
		Thread threadFond = new Thread(jPanelFond);

		DeplacementMeteorite dm = new DeplacementMeteorite(this, listeMeteorites, score);
		Thread threadDeplacementMeteorite = new Thread(dm);
		CreationMeteoriteService cms = new CreationMeteoriteService(this);
		Thread threadCreation = new Thread(cms);

		Thread threadTir = new Thread(new TirService(this));

		// Demarrage de tous les threads
		threadFond.start();
		threadDeplacementMeteorite.start();
		threadCreation.start();
		threadTir.start();
	}

	@Override
	public void run() {
		while (true) {
			score.setNom("");
			JDialog boiteDialogueDemmarage = new FenetreDialDemarrage(this);
			boiteDialogueDemmarage.setVisible(true);
			while ("".equals(score.getNom())) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			boiteDialogueDemmarage.setVisible(false);
			do {
				demarrerJeu();
				do {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} while (ControleVie.estVivant(avion));

				new FichierService().ecritureFichier(score);

			} while (JOptionPane.showConfirmDialog(null, "Score : " + score.getScorePoints() + "\nNouvelle Partie?",
					"Fin de partie ", JOptionPane.YES_NO_OPTION) == 0);
		}

	}

}
