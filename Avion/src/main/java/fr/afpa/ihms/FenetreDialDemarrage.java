package fr.afpa.ihms;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import fr.afpa.listeners.BoiteDialStartListener;

public class FenetreDialDemarrage extends JDialog{
	private FenetrePrincipale fenetre;
	
	public FenetreDialDemarrage(FenetrePrincipale fenetre) {
		super(fenetre);
		this.fenetre = fenetre;
		ActionListener ListenerDebut=new BoiteDialStartListener(fenetre, this);
		JButton newGame=new JButton("NEW GAME");
		newGame.addActionListener(ListenerDebut);
		JButton afficherScore=new JButton("TOP SCORES");
		afficherScore.addActionListener(ListenerDebut);
		JButton quitter=new JButton("QUITTER");
		quitter.addActionListener(ListenerDebut);
		JPanel x=new JPanel(new GridLayout(3, 1));
		x.add(newGame);
		x.add(afficherScore);
		x.add(quitter);
		add(x);
		//this.setLocation(fenetre.getX()+220, fenetre.getY()+275);
		this.pack();
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLocationRelativeTo(this.fenetre);
	}

	
	
}

		
	

