package fr.afpa.services;


import fr.afpa.entites.Avion;
import fr.afpa.entites.Meteorite;
import fr.afpa.ihms.FenetrePrincipale;

public class MeteoriteService {

	/**
	 * Permet de gerer l'eventuelle collision entre l'avion et une meteorite.
	 * S'il y a collision, le calcul des points des dommages est effectue.
	 * @param avion : l'avion du joueur
	 * @param meteorite : la meteorite qui peut potentiellement percuter l'avion
	 * @param fenetre : la fenetre qui contient l'avion et la meteorite
	 * @return true si l'avion rencontre une meteorite et false sinon
	 */
	public boolean collision(Avion avion, Meteorite meteorite, FenetrePrincipale fenetre) {

		if(avion.getBounds().intersects(meteorite.getBounds())) {

			avion.setPv(avion.getPv() - meteorite.getDegatMeteorite());
			fenetre.getJLabelVie().setText("Vie : "+ avion.getPv());
			avion.setImageEnCours(avion.getExplosion1());
			meteorite.setMeteoritePoint(false);
			meteorite.setLocation(0, 1000);
			fenetre.repaint();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			}
			avion.setImageEnCours(avion.getAvionCentre());
			fenetre.repaint();
			return true;
		}
		return false;
	}
}

	
