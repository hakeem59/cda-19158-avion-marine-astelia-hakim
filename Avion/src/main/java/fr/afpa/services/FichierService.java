package fr.afpa.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.entites.Score;

public class FichierService {
	
	private static final String MESSAGE_ERREUR = "Tant que vous ne creez pas la variable d'environnement 'avion'"
			+ "\n avec pour chemin le fichier texte de sauvegarde, "
			+ "\n vous ne pourrez pas sauvegarder votre score ni les lire.";
	
	/**
	 * Permet d'ecrire un score dans un fichier
	 * @param score : le nom, le score et la date a laquelle est commencee la partie
	 */
	public void ecritureFichier(Score score) {
		String nom1=score.getNom();
		int score1=score.getScorePoints();
		LocalDateTime date1=score.getDate();
		try(FileWriter fw=new FileWriter(System.getenv("avion"),true);BufferedWriter bw=new BufferedWriter(fw)) {
	
			bw.write(nom1+";"+score1+";"+date1);
			bw.newLine();
			
		} catch (Exception e) {
			System.out.println(MESSAGE_ERREUR);
		}
	}
	
	/**
	 * Permet de lire une liste de scores
	 * @return la liste des scores avec les noms des joueurs, leurs scores et les dates de jeu
	 */
	public List<String> lectureFichier() {
		
		List<String> scoreJoueur=new ArrayList();
		try(FileReader fr = new FileReader(System.getenv("avion"));BufferedReader br=new BufferedReader(fr);) {
		
		
		while(br.ready())
		scoreJoueur.add(br.readLine());
		} 
		catch (Exception e) {
			System.out.println(MESSAGE_ERREUR);
}
		return scoreJoueur;
	}
}
