package fr.afpa.services;

import java.util.ArrayList;
import java.util.List;
import fr.afpa.controle.ControleBordures;
import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Meteorite;
import fr.afpa.entites.Score;
import fr.afpa.ihms.FenetrePrincipale;

public class DeplacementMeteorite implements Runnable {

	private FenetrePrincipale fenetre;
	private List<Meteorite> listeMeteorites;
	private Score score;

	public DeplacementMeteorite(FenetrePrincipale fenetre, List<Meteorite> liste, Score score) {
		this.fenetre = fenetre;
		this.listeMeteorites = liste;
		this.score = score;
	}
	
	/**
	 * Permet de gerer les deplacements des meteorites.
	 * Les meteorites sont retirees de la liste des meteorites lorsqu'elles disparaissent
	 * de l'ecran.
	 * @return true si la fonction s'est deroulee normalement et false sinon
	 */
	public boolean gestionDeplacementMeteorite() {
		List<Meteorite> listeARetirer = new ArrayList<Meteorite>();
		int pas = fenetre.getJPanelFond().getPasIncrementation();
		for (Meteorite meteorite : listeMeteorites) {
			DeplacementService.deplacer(fenetre, meteorite,
					meteorite.getDx() * pas * meteorite.getVitesse(),
					meteorite.getDy() * pas * meteorite.getVitesse());
			if (ControleBordures.sousLeBordDuBas(fenetre, meteorite)) {
				listeARetirer.add(meteorite);
			}
			new MeteoriteService().collision(fenetre.getAvion(), meteorite, fenetre);
		}
		try {
			for (int i = 0; i < listeARetirer.size(); i++) {
				new ScoreService().ajoutPointsScores(fenetre, score, listeARetirer.get(i));
				fenetre.getJPanelFond().remove(listeARetirer.get(i));
				listeMeteorites.removeAll(listeARetirer);
			}
			fenetre.getJLabelScore().setText("Score : " + score.getScorePoints());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public synchronized void run() {
		while (ControleVie.estVivant(fenetre.getAvion())) {

			if (fenetre.isPause()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				synchronized (fenetre) {
					gestionDeplacementMeteorite();
				}
				try {
					Thread.sleep(0, fenetre.getJPanelFond().getVitesseJeu());
				} catch (InterruptedException e) {
					System.out.println("Probleme dans le deplacement de la meteorite !");
				}
			}
		}
	}

}
