package fr.afpa.services;

import java.util.Random;

import javax.swing.JPanel;

import fr.afpa.controle.ControleBordures;
import fr.afpa.entites.MeteoriteZigZag;
import fr.afpa.ihms.FenetrePrincipale;

public class DeplacementService {

	/**
	 * Permet de bouger la position d'un JPanel
	 * 
	 * @param fenetre : la fenetre qui contient le JPanel
	 * @param JPanel  : le panel a bouger
	 * @param dx      : l'abscisse a ajouter
	 * @param dy      : l'ordonnee a ajouter
	 */
	public static void deplacer(FenetrePrincipale fenetre, JPanel panel, int dx, int dy) {

		if (panel instanceof MeteoriteZigZag) {
			((MeteoriteZigZag) panel).setCompteur(((MeteoriteZigZag) panel).getCompteur() + 1);
			if (((MeteoriteZigZag) panel).getCompteur() % 100 == 0) {
				int aleatoire = new Random().nextInt(100);
				if (aleatoire % 2 == 0) {
					((MeteoriteZigZag) panel).setDeplacement(!((MeteoriteZigZag) panel).isDeplacement());
				}

			}
			if (((MeteoriteZigZag) panel).isDeplacement()) {
				dx = -dx;
			}
		}
		if (ControleBordures.dansLeCadre(fenetre, panel, panel.getX() + dx, panel.getY() + dy)) {
			panel.setLocation(panel.getX() + dx, panel.getY() + dy);
		}
	}

}
