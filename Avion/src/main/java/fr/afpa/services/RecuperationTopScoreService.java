package fr.afpa.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.afpa.controle.ControleDate;
import fr.afpa.entites.Score;

public class RecuperationTopScoreService {
	/**
	 * Retourne une chaine de caractères representant la liste des meilleurs scores
	 * 
	 * @param nbScore : le nombre de scores à afficher
	 * @return une chaine de caractères représentant le top des meilleurs Scores
	 */
	public String affichageTop(int nbScore) {
		List<Score> listeScores = recuperationTopScores(nbScore);
		StringBuilder builder = new StringBuilder();
		int position=0;
		builder.append(String.format("%-5s", " ")+" | " + String.format("%-15s", "Nom")+" | " + String.format("%-15s", "Point")+" | "
		+ String.format("%-15s", "Date")+ "\n");
		builder.append("***************************************************"+ "\n");
		for (Score score : listeScores) {
			builder.append(String.format("%-5s", ++position)+" | " + String.format("%-15s", score.getNom())+" | " + String.format("%-15s", score.getScorePoints())
			+" | "+ String.format("%-15s", score.getDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")))
					+ "\n");
		}
		return builder.toString();

	}

	/**
	 * Récuperation de la liste des scores et tri de cette liste
	 * 
	 * @param nbScore : le nombre scores à afficher
	 * @return : une liste des meilleurs scores
	 */
	private List<Score> recuperationTopScores(int nbScore) {
		List<String> ligneFichier = new FichierService().lectureFichier();
		List<Score> listeScores = new ArrayList<Score>();
		for (String ligne : ligneFichier) {
			String[] ligneSplit = ligne.split(";");
			if (ligneSplit.length == 3 && ligneSplit[1].matches("[0-9]+")
					&& ControleDate.estDateValide(ligneSplit[2])) {
				listeScores.add(
						new Score(ligneSplit[0], Integer.parseInt(ligneSplit[1]), LocalDateTime.parse(ligneSplit[2])));
			}
		}
		Collections.sort(listeScores);
		if (listeScores.size() > 20) {
			return listeScores.subList(0, nbScore);
		} else {
			return listeScores;
		}
	}

}
