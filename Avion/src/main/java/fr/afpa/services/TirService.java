package fr.afpa.services;

import java.awt.Rectangle;
import java.util.Iterator;

import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Boulet;
import fr.afpa.entites.Meteorite;
import fr.afpa.ihms.FenetrePrincipale;

public class TirService implements Runnable {

	private FenetrePrincipale fenetre;

	public TirService(FenetrePrincipale fenetre) {
		this.fenetre = fenetre;
	}

	/**
	 * Permet de gerer si un boulet entre en collision avec des meteorites
	 * @param boulet : le boulet gere
	 * @return true si le boulet a rencontre une meteorite et false sinon
	 */
	public boolean collisionMeteorite(Rectangle boulet) {
		Iterator<Meteorite> it = fenetre.getListeMeteorites().iterator();
		while (it.hasNext()) {
			Meteorite meteorite = it.next();
			if (boulet.intersects(meteorite.getBounds())) {
				meteorite.setLocation(0, 1000);
				return true;
			}

		}

		return false;
	}

	@Override
	public void run() {
		while (ControleVie.estVivant(fenetre.getAvion())) {
			if (fenetre.isPause()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				synchronized (fenetre) {
					int pas = fenetre.getJPanelFond().getPasIncrementation();
					Iterator<Boulet> it = fenetre.getListeBoulets().iterator();
					while (it.hasNext()) {
						Boulet boulet = it.next();
						DeplacementService.deplacer(fenetre, boulet, 0, -pas * 20);
						if (boulet.getY() < 0 || collisionMeteorite(boulet.getBounds())) {
							fenetre.getJPanelFond().remove(boulet);
							it.remove();
						}
						fenetre.repaint();
					}
				}

				try {
					Thread.sleep(fenetre.getJPanelFond().getVitesseJeu());
				} catch (InterruptedException e) {
					System.out.println("Probleme dans le deplacement des boulets");
				}
			}

		}

	}
}
