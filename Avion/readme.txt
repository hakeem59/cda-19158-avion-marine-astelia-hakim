Projet : Avion
Auteurs : Astelia Massamba, Hakim Ben Amara, Marine Zimmer
Descripition : jeu de l'avion, le but du jeu et d'�viter les m�t�orites

Technologies et outils utilis�es : Java / Maven/ Lombok / Gitkraken / Bitbucket

Environement n�cessaire : Java Runtime Environment 8

D�marche � suivre :
1)Cr�er une variable d'environnement avion avec comme valeur le chemin d'acc�s du fichier de sauvegarde des scores (par exemple C:\ENV\avion\sauvegarde.txt)
1) Sur le r�pertoire C cr�er l�arborescence suivante 

(nom du jar).jar se trouve dans le dossier ... 
Mettre le dossier ressources dans le dossier avion 

	ENV 
   	|------avion 
		 |
		 |------(nom du jar).jar 
		 |-------ressources 

  

  

2) Ouvrir l�invite de commande (cmd dans la barre de recherche). 

3) D�marrage de l�application, taper les commandes suivantes :  
cd C:\ENV\avion 
java -jar (nom du jar).jar 

Fonctionnement du jeu : 
  
La fen�tre de jeu s'affiche le joueur a le choix entre : 
	- D�marrer une nouvelle partie 
	- Afficher les 20 meilleurs scores 
	- Quitter 

Lorsque le joueur d�cide de d�marrer une nouvelle partie, il doit entrer son nom dans la boite de dialogue. 
  
Une fois un nom correct entr�, la partie se lance. 
Le but du jeu est de d�placer le vaisseau afin d'�viter les m�t�orites. 
Le joueur peut se d�placer avec les touches directionnelles ou les touches Q, Z, D, S. 
Le joueur a la possibilit� de tirer sur les m�t�orites avec la barre espace 
A chaque fois que le joueur percute une m�t�orite, il perd un certain nombre de points de vie selon le type de m�t�orite 
A chaque fois qu'une m�t�orite passe la ligne du bas ou tir sur une m�t�orite, le joueur gagne des points selon le type de m�t�orite. 
Le Joueur a la possibilit� de mettre le jeu en pause avec la touche B 

Il y a 5 types de m�t�orites :  

	- M�t�orite de base : 1 point de d�g�t et 2 points 
	- M�t�orite de feu : 2 points de d�g�t et 1 point 
	- M�t�orite de glace : 2 points de d�g�t et 3 points 
	- M�t�orite zigzag (se d�place al�atoirement en zig zag) : 2 points de d�g�t et 5 points 
	- M�t�orite iceberg : 4 points de d�g�t et 5 points 

Le jeu s'acc�l�re en fonction du score du joueur 
Le jeu se termine lorsque le joueur n'a plus de vie. 
Une fois que le joueur a atteint les 999 points, le nombre de points n'augmente plus mais il peut continuer � jouer 

Horaires de fonctionnement du programme : 24h/24, 7j/7 



